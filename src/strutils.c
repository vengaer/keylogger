#include "strutils.h"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

int strscpy(char* restrict dst, char const* restrict src, size_t count) {
    size_t const src_len = strlen(src);

    strncpy(dst, src, count);
    dst[count - 1] = '\0';

    if(src_len >= count)
        return -E2BIG;

    return src_len;
}

int strsncpy(char* restrict dst, char const* restrict src, size_t count, size_t n) {
    size_t const len = n < count ? n : count;

    strncpy(dst, src, len);
    dst[count - 1] = '\0';

    if(n >= count)
        return -E2BIG;

    return n;
}

int strscat(char* restrict dst, char const* restrict src, size_t count) {
    size_t const src_len = strlen(src);
    size_t const dst_len = strlen(dst);

    strncat(dst, src, count - dst_len);
    dst[count - 1] = '\0';

    if(src_len >= count - dst_len)
        return -E2BIG;

    return src_len;
}

int strscatf(char* restrict dst, size_t count, char const* restrict fmt, ...) {
    size_t const dst_len = strlen(dst);

    va_list args;
    va_start(args, fmt);
    vsnprintf(dst + dst_len, count - dst_len, fmt, args);
    va_end(args);

    /* Not null-terminated */
    if(dst[count - 1]) {
        dst[count - 1] = '\0';
        return -E2BIG;
    }

    return strlen(dst) - dst_len;
}
