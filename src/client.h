#ifndef CLIENT_H
#define CLIENT_H

int client_send_msg(char const* restrict msg, char const* restrict dst_ip);

#endif
