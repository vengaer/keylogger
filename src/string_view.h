#ifndef STRING_VIEW_H
#define STRING_VIEW_H

#include <stddef.h>

#include <regex.h>

struct string_view {
    char const* data;
    size_t size;
};

static inline void regmatch_to_string_view(char const* seq, regmatch_t* pmatch, size_t idx, struct string_view* str) {
    str->data = seq + pmatch[idx].rm_so;
    str->size = pmatch[idx].rm_eo - pmatch[idx].rm_so;
}

#endif
