#ifndef SOCKET_UTILS_H
#define SOCKET_UTILS_H

#define PORT 11223u
#define SEND_BUF_SIZE 512
#define PROTOCOL "tcp"
#define DEFAULT_SERVER_IP "127.0.0.1"

int is_valid_ip_addr(char const* ip);
void close_socket(int* fd);

#endif
