#include "client.h"
#include "socket_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>

#ifndef __GNUC__
#error GNU C support required
#endif

int client_send_msg(char const* restrict msg, char const* restrict dst_ip) {
    struct protoent* protoent;
    int sockfd __attribute__ ((__cleanup__(close_socket)));
    in_addr_t in_addr;
    struct hostent* hostent;
    struct sockaddr_in sockaddr_in;

    protoent = getprotobyname(PROTOCOL);
    if(!protoent) {
        fprintf(stderr, "Failed to get protocol\n");
        return 1;
    }

    sockfd = socket(AF_INET, SOCK_STREAM, protoent->p_proto);
    if(sockfd == -1) {
        fprintf(stderr, "Failed to open socket\n");
        return 1;
    }
    if(dst_ip && is_valid_ip_addr(dst_ip)) {
        hostent = gethostbyname(dst_ip);
    }
    else if(dst_ip) {
        fprintf(stderr, "%s is not a valid ip address\n", dst_ip);
        return 1;
    }
    else {
        hostent = gethostbyname(DEFAULT_SERVER_IP);
    }

    if(!hostent) {
        fprintf(stderr, "Failed to get host %s\n", dst_ip ? dst_ip : DEFAULT_SERVER_IP);
        return 1;
    }

    in_addr = inet_addr(inet_ntoa(*(struct in_addr*)*(hostent->h_addr_list)));

    if(in_addr == (in_addr_t) - 1) {
        fprintf(stderr, "Failed to set inet address\n");
        return 1;
    }

    sockaddr_in.sin_addr.s_addr = in_addr;
    sockaddr_in.sin_family = AF_INET;
    sockaddr_in.sin_port = htons(PORT);

    if(connect(sockfd, (struct sockaddr*)&sockaddr_in, sizeof(sockaddr_in)) == -1) {
        fprintf(stderr, "Failed to connect to socket\n");
        return 1;
    }

    if(write(sockfd, msg, strlen(msg)) == -1) {
        fprintf(stderr, "Failed to write to server\n");
        return 1;
    }
    return 0;
}

