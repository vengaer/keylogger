#ifndef STRUTILS_H
#define STRUTILS_H

#include <stddef.h>

#ifndef E2BIG
#define E2BIG 7
#endif

int strscpy(char* restrict dst, char const* restrict src, size_t count);
int strsncpy(char* restrict dst, char const* restrict src, size_t count, size_t n);
int strscat(char* restrict dst, char const* restrict src, size_t count);
int strscatf(char* restrict dst, size_t count, char const* restrict fmt, ...);

#endif
