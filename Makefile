CC          ?= gcc

TARGET      := keylogger

SRC_DIR     := src
BUILD_DIR   := build
SRC_EXT     := c
OBJ_EXT     := o

CFLAGS      := -std=c11 -Wall -Wextra -pedantic -Wshadow -Wunknown-pragmas
LIB         :=

SRC         := $(shell find $(SRC_DIR) -mindepth 1 -type f -name *.$(SRC_EXT))
OBJ         := $(patsubst $(SRC_DIR)/%, $(BUILD_DIR)/%, $(SRC:.$(SRC_EXT)=.$(OBJ_EXT)))

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) -o $@ $^ $(LIB)

$(BUILD_DIR)/%.$(OBJ_EXT): $(SRC_DIR)/%.$(SRC_EXT) dirs
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: run clean dirs

run: $(TARGET)
	@./$(TARGET)

clean:
	rm -f $(OBJ) $(TARGET); rm -rf $(BUILD_DIR)

dirs:
	@mkdir -p $(BUILD_DIR)
